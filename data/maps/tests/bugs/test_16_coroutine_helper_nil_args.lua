-- This unit test verifies arguments of nil can be passed to the coroutine helper script

require("scripts/coroutine_helper")

local map = ...
local game = map:get_game()
local hero = game:get_hero()

function map:on_opening_transition_finished()
    map:start_coroutine(function()
        wait_for(hero.start_treasure, hero, "heart", 1, nil)
        sol.main.exit()
    end)
end
