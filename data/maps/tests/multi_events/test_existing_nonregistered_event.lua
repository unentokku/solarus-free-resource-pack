--[[
	This unit test of the multi_events script creates a sprite with the
	'on_direction_changed()' event first defined the normal way then registers some
	additional instances of `on_direction_changed()` using the multi-events script.
]]

local map = ...
local game = map:get_game()

require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

function map:on_opening_transition_finished()
	local sprite = sol.sprite.create"hero/tunic1"
	events_proto:set_trigger(function() --will call this function to trigger the events
		sprite:set_direction(1 - sprite:get_direction()) --toggle between direction 1 & 0
	end)
	
	
	--Test #1: define a regular event not using multi-events
	function sprite:on_direction_changed()
		events_proto:log"non-registered"
	end
	events_proto:trigger("non-registered", "A normally created event did not trigger")
	
	
	--Test #2: register a new event with multi-events, new event should come after existing
	sprite:register_event("on_direction_changed", function()
		events_proto:log"sprite1"
	end)
	events_proto:trigger"non-registered;sprite1"
	
	
	--Test #3: register another event with multi-events, new event should come after existing
	sprite:register_event("on_direction_changed", function()
		events_proto:log"sprite2"
	end)
	events_proto:trigger"non-registered;sprite1;sprite2"
	
	
	--Test #4: register a 3rd event using multi-events, this one should be before all others
	sprite:register_event("on_direction_changed", function()
		events_proto:log"sprite3"
	end, true) --make this event first
	events_proto:trigger"sprite3;non-registered;sprite1;sprite2"
	
	
	--Test #5: now trying to register an event the normal way should cause error
	local status, err = pcall(function()
		function sprite:on_direction_changed() events_proto:log"bad_event" end
	end)
	assert(status==false, "Defining an event normally after using register_event() did not throw an error")
	events_proto:trigger("sprite3;non-registered;sprite1;sprite2", "sequence should not contain 'bad_event'")
	
	
	--Test #6: define meta event
	local sprite_meta = sol.main.get_metatable"sprite"
	function sprite_meta:on_direction_changed()
		events_proto:log"meta"
	end
	events_proto:trigger"sprite3;non-registered;sprite1;sprite2"
	
	
	--Test #7: register event in-front of non-registered event
	local enemy = map:create_enemy{
		x=256, y=32, layer=0,
		direction = 0,
		breed = "skullbat",
	}
	events_proto:set_trigger(function() enemy:set_life(0) end)
	function enemy:on_dying(attack) events_proto:log"non-registered_enemy" end
	enemy:register_event("on_dying", function(attack) events_proto:log"enemy1" end, true)
	events_proto:trigger"enemy1;non-registered_enemy"
	
	
	events_proto:exit()
end
