--[[
	This unit test for the multi_events script verifies that the
	door:on_opened() event is triggered.
]]

local map = ...
local game = map:get_game()
local hero = game:get_hero()

require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

local end_test_cb
door:register_event("on_opened", function(self)
	events_proto:log(sol.main.get_type(self))
end)

function map:on_opening_transition_finished()
	events_proto:set_trigger(function(callback)
		end_test_cb = callback
		door:set_open()
		return true
	end)
	events_proto:trigger"door"
	
	sol.timer.start(self, 10, function()
		end_test_cb()
		events_proto:exit() --verifies all tests have finished running before exit
	end)
end
