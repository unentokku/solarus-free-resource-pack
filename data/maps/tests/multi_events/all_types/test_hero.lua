--[[
	This unit test for the multi_events script verifies that the hero:on_taking_damage
	event gets triggered.
]]

local map = ...
local game = map:get_game()
local hero = game:get_hero()

require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

hero:register_event("on_taking_damage", function(self, damage)
	local data = {sol.main.get_type(self), damage}
	events_proto:log(table.concat(data, ","))
end)

function map:on_opening_transition_finished()
	events_proto:set_trigger(function()
		hero:start_hurt(1)
	end)
	events_proto:trigger"hero,1"
	
	events_proto:exit() --verifies all tests have finished running before exit
end
