--[[
	This unit test for the multi_events script verifies that the item:on_pickable_created
	event gets triggered.
]]

local map = ...
local game = map:get_game()
local hero = game:get_hero()

require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

local item = game:get_item"gem"
item:register_event("on_pickable_created", function(self, pickable)
	local pickable_name = pickable:get_name()
	local data = {sol.main.get_type(self), self:get_name(), pickable_name}
	events_proto:log(table.concat(data, ","))
end)

function map:on_opening_transition_finished()
	events_proto:set_trigger(function()
		map:create_pickable{
			name = "my_gem",
			x = 88,
			y = 16,
			layer = 1,
			treasure_name = "gem",
			treasure_variant = 1,
		}
	end)
	events_proto:trigger"item,gem,my_gem"
	
	events_proto:exit() --verifies all tests have finished running before exit
end
