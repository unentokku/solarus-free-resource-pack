--[[
	This unit test for the multi_events script verifies that the
	map:on_obtaining_treasure() event is triggered.
]]

local map = ...
local game = map:get_game()
local hero = game:get_hero()

require"scripts/multi_events"
local events_proto = require"tests/events_prototype"

game:get_item"heart":set_brandish_when_picked(false)
map:register_event("on_obtaining_treasure", function(self, item, variant, savegame_variable)
	local data = {sol.main.get_type(self), item:get_name(), variant, savegame_variable}
	events_proto:log(table.concat(data, ","))
end)

function map:on_opening_transition_finished()
	events_proto:set_trigger(function()
		hero:start_treasure("heart", 1, "save_name")
	end)
	events_proto:trigger"map,heart,1,save_name"
	
	events_proto:exit() --verifies all tests have finished running before exit
end
