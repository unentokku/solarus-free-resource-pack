# Changelog

## Solarus Free Resource Pack 1.6.4 (2020-04-11)

* Update OceanSet tilesets.
* Fix sword running tunic animation.

## Solarus Free Resource Pack 1.6.3 (2020-03-20)

* Update Ocean's Hearts tilesets.

## Solarus Free Resource Pack 1.6.2 (2019-08-15)

* Remove two broken enemy scripts.
* Fix wrong license of some sprite files.

## Solarus Free Resource Pack 1.6.1 (2019-08-10)

* Add dungeon music from Eduardo.
* Fix HUD no longer displaying after game-over.
* Fix Lua error when life is low (#21).
* Fix CRT-interlaced shader not compiling with some GLSL versions.
* Remove image with proprietary content added by mistake (#13)

## Solarus Free Resource Pack 1.6.0 (2018-12-22)

* Initial release
